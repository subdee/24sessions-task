$(function () {
    const ipElement = $('#ip_address');
    const cityElement = $('#city');
    const countryElement = $('#country');
    const errorElement = $('#error');
    const btnElement = $('#get_geolocation_btn');

    $.ajax({
        url: 'https://ipv4.icanhazip.com',
        success: function (data) {
            ipElement.text(data);
        }
    });

    btnElement.on('click', function () {
        btnElement.text('Retrieving...');
        btnElement.attr('disabled', true);
        $.ajax({
            url: 'http://localhost:9000/geolocation',
            method: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({ipAddress: ipElement.text().trim()}),
            success: function (data) {
                cityElement.text(data.result.city);
                countryElement.text(data.result.country);
            },
            error: function (e) {
                if (e.responseJSON !== undefined) {
                    errorElement.text('Could not retrieve geolocation info because: ' + e.responseJSON.message);
                } else {
                    errorElement.text('Could not retrieve geolocation info because of an unknown reason');
                }
            },
            complete: function () {
                btnElement.text('Get geolocation information');
                btnElement.attr('disabled', false);
            }
        })
    });
});
