<?php

namespace subdee\Services;

use DavidePastore\Ipinfo\Ipinfo;
use subdee\DAO\Geolocation as GeolocationDao;

class IpInfoProvider
{
    private $client;
    private $cacheProvider;
    private $geolocationService;

    public function __construct(
        Ipinfo $client,
        CacheProviderInterface $cacheProvider,
        GeolocationService $geolocationService
    )
    {
        $this->client = $client;
        $this->cacheProvider = $cacheProvider;
        $this->geolocationService = $geolocationService;
    }

    public function getGeolocation(string $ipAddress): GeolocationDao
    {
        $geolocation = $this->cacheProvider->get($ipAddress);

        if ($geolocation !== null) {
            return GeolocationDao::create($ipAddress, $geolocation->city, $geolocation->country);
        }

        $details = $this->client->getIpGeoDetails($ipAddress);
        $this->cacheProvider->save(
            $this->geolocationService->create($ipAddress, $details->getCity(), $details->getCountry())
        );

        return GeolocationDao::create($ipAddress, $details->getCity(), $details->getCountry());
    }
}
