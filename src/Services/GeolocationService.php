<?php

namespace subdee\Services;


use subdee\Models\Geolocation;

class GeolocationService
{
    public function create(string $ipAddress, string $city, string $country): Geolocation
    {
        return Geolocation::hydrate($ipAddress, $city, $country);
    }
}
