<?php

namespace subdee\Services;


use ActiveRecord\Model;
use subdee\Models\Geolocation;
use subdee\Repositories\GeolocationRepository;

class DbCacheProvider implements CacheProviderInterface
{
    private $geolocationRepository;

    public function __construct(GeolocationRepository $geolocationRepository)
    {
        $this->geolocationRepository = $geolocationRepository;
    }

    public function get(string $key): ?Geolocation
    {
        $geolocation = $this->geolocationRepository->find($key);

        if ($geolocation === null) {
            return null;
        }

        return $geolocation;
    }

    public function save(Model $geolocation)
    {
        $geolocation->save();
    }
}
