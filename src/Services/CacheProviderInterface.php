<?php

namespace subdee\Services;


use ActiveRecord\Model;
use subdee\Models\Geolocation;

interface CacheProviderInterface
{
    public function get(string $key): ?Geolocation;

    public function save(Model $key);
}
