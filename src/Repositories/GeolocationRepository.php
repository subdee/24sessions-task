<?php

namespace subdee\Repositories;


use PDO;
use subdee\Models\Geolocation;

class GeolocationRepository
{
    public function find(string $ipAddress): ?Geolocation
    {
        /** @var \PDOStatement $query */
        $query = Geolocation::query(<<<SQL
SELECT * FROM geolocation WHERE ip_address = ?
SQL
            , [$ipAddress]);

        $result = $query->fetch(PDO::FETCH_ASSOC);

        if ($result === false) {
            return null;
        }

        return Geolocation::hydrate($result['ip_address'], $result['city'], $result['country']);
    }
}
