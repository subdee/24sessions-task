<?php

namespace subdee\Models;


use ActiveRecord\Model;

/**
 * @property string ip_address
 * @property string city
 * @property string country
 */
class Geolocation extends Model
{
    static $table_name = 'geolocation';

    public static function hydrate(string $ipAddress, string $city, string $country): self
    {
        $obj = new self([
            'ip_address' => $ipAddress,
            'city' => $city,
            'country' => $country,
        ]);

        return $obj;
    }
}
