<?php

namespace subdee\Helpers;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class JsonRequestTransformer
{
    public static function transform(Request $request): Request
    {
        //Check to see if the correct Content-Type is provided
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);

            return $request;
        }

        throw new NotAcceptableHttpException('Content type not allowed');
    }
}
