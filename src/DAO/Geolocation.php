<?php

namespace subdee\DAO;


class Geolocation
{
    private $ipAddress;
    private $city;
    private $country;

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public static function create(string $ipAddress, string $city, string $country): self
    {
        $dao = new self();
        $dao->ipAddress = $ipAddress;
        $dao->city = $city;
        $dao->country = $country;

        return $dao;
    }
}
