<?php

namespace subdee\Controllers;


use subdee\Services\IpInfoProvider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController
{
    private $ipInfoProvider;
    private $serializer;

    public function __construct(IpInfoProvider $ipInfoProvider, SerializerInterface $serializer)
    {
        $this->ipInfoProvider = $ipInfoProvider;
        $this->serializer = $serializer;
    }

    public function getAction(Request $request): Response
    {
        $ipAddress = $request->request->get('ipAddress');

        if (filter_var($ipAddress, FILTER_VALIDATE_IP) === false) {
            return new JsonResponse(['success' => false, 'message' => 'IP address improperly formatted'], 400);
        }

        $geolocation = $this->ipInfoProvider->getGeolocation($ipAddress);

        return new JsonResponse([
            'success' => true,
            'result' => [
                'city' => $geolocation->getCity(),
                'country' => $geolocation->getCountry(),
            ]
        ]);
    }
}
