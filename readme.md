# IP Info Puppy

## Description

This application is a simple page that detects your public IP address and with a click of a button finds out your city and country. It uses the info.io API to grab the geolocation information.

## Requirements

- PHP 7.1+
- SQLite database named database.db, placed in data folder, with a table called `geolocation`. Oh yea, and three fields, `ip_address`, `city` and `country`
- Modern web browser with Javascript enabled
- Patience

## Installation

Clone the project to a folder of your choice. Go into that folder and run the API using PHP built-in server:

```bash
git clone git@gitlab.com:subdee/24sessions-task.git
cd 24sessions-task
php -S localhost:9000
```

Then open the [index.html](frontend/publicindex.html) file with your modern web browser and voila!

## Issues

> It doesn't work!!one!!1!

Well, make sure you have the requirements setup

> Nope, your app sucks!

You're right. Move along!

## Contribute

It's perfect already, no need!
