<?php

use ActiveRecord\Config;
use DavidePastore\Ipinfo\Ipinfo;
use Silex\Application;
use Silex\Provider\SerializerServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use subdee\Controllers\ApiController;
use subdee\Helpers\JsonRequestTransformer;
use subdee\Repositories\GeolocationRepository;
use subdee\Services\DbCacheProvider;
use subdee\Services\GeolocationService;
use subdee\Services\IpInfoProvider;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . '/vendor/autoload.php';

Config::initialize(function (Config $cfg) {
    $cfg->set_model_directory(__DIR__ . '/src/Models');
    $cfg->set_connections(
        array(
            'development' => 'sqlite://data/database.db',
            'production' => 'mysql://test_user:secret@localhost/test'
        )
    );
});

ErrorHandler::register();
$app = new Application();
$app['debug'] = true;
$app->register(new ServiceControllerServiceProvider());
$app->register(new SerializerServiceProvider());
$app['geolocation.repository'] = function () {
    return new GeolocationRepository();
};
$app['ipInfo.client'] = function () {
    return new Ipinfo(['token' => 'e4294a9af70e2b']);
};
$app['db.cache.provider'] = function () use ($app) {
    return new DbCacheProvider($app['geolocation.repository']);
};
$app['geolocation.service'] = function () use ($app) {
    return new GeolocationService();
};
$app['ipInfo.provider'] = function () use ($app) {
    return new IpInfoProvider($app['ipInfo.client'], $app['db.cache.provider'], $app['geolocation.service']);
};
$app['api.controller'] = function () use ($app) {
    return new ApiController($app['ipInfo.provider'], $app['serializer']);
};
$app->before(function (Request $request) {
    return JsonRequestTransformer::transform($request);
});
$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
});
$app->options("{anything}", function () {
    return new JsonResponse(null, 204);
})->assert("anything", ".*");
$app->error(function (Exception $e) use ($app) {
    $error = array('message' => $e->getMessage());

    return $app->json($error, 400);
});
$app->post('/geolocation', 'api.controller:getAction');

$app->run();
