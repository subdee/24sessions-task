<?php

use DavidePastore\Ipinfo\Ipinfo;
use PHPUnit\Framework\TestCase;
use subdee\DAO\Geolocation;
use subdee\Repositories\GeolocationRepository;
use subdee\Services\DbCacheProvider;
use subdee\Services\GeolocationService;

class IpInfoProviderTest extends TestCase
{
    /** @var GeolocationRepository|\Mockery\MockInterface */
    private $mockClient;

    /** @var \subdee\Services\CacheProviderInterface|\Mockery\MockInterface */
    private $mockCacheProvider;

    /** @var GeolocationService|\Mockery\MockInterface */
    private $mockGeolocationService;

    public function setUp()
    {
        $this->mockClient = \Mockery::mock(Ipinfo::class);
        $this->mockCacheProvider = \Mockery::mock(DbCacheProvider::class);
        $this->mockGeolocationService = \Mockery::mock(GeolocationService::class);
    }

    public function testGetGeolocationWithValidIpAddressWithoutCache()
    {
        $host = new \DavidePastore\Ipinfo\Host([
            'city' => 'Kalamata',
            'country' => 'GR'
        ]);

        $this->mockClient->shouldReceive('getIpGeoDetails')->with('127.0.0.1')->andReturn($host);
        $this->mockGeolocationService->shouldReceive('create');
        $this->mockCacheProvider->shouldReceive('save');
        $this->mockCacheProvider->shouldReceive('get')->andReturnNull();

        $ipInfoProvider = new \subdee\Services\IpInfoProvider(
            $this->mockClient,
            $this->mockCacheProvider,
            $this->mockGeolocationService
        );
        $geolocation = $ipInfoProvider->getGeolocation('127.0.0.1');

        $this->assertInstanceOf(Geolocation::class, $geolocation);
        $this->assertEquals('Kalamata', $geolocation->getCity());
    }
}
