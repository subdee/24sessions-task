<?php

use PHPUnit\Framework\TestCase;
use subdee\Helpers\JsonRequestTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class JsonRequestTransformerTest extends TestCase
{
    public function testTransformsJsonInputToAssocArray()
    {
        $request = Request::create(
            'testuri',
            'get',
            [],
            [],
            [],
            [],
            '{"testing":1}'
        );
        $request->headers->set('Content-Type', 'application/json');
        JsonRequestTransformer::transform($request);

        $this->assertEquals(1, $request->request->get('testing'));
    }

    public function testTransformsOtherInputToException()
    {
        $request = Request::create(
            'testuri',
            'get',
            [],
            [],
            [],
            [],
            '<root>hello</root>'
        );
        $request->headers->set('Content-Type', 'application/xml');
        $this->expectException(NotAcceptableHttpException::class);
        JsonRequestTransformer::transform($request);
    }
}
